# Sorts alist silently
def heapSort(alist):
    length = len(alist) - 1
    leastParent = length/2
    for i in range(leastParent, -1,-1):
        moveDown(alist,i,length)
    for i in range(length,0,-1):
        if alist[0]>alist[i]:
            swap(alist,0,i)
            moveDown(alist,0,i-1)

def moveDown(alist,first,last):
    largest = 2*first+1
    while largest <= last:
        if (largest<last) and (alist[largest] < alist[largest+1]):
            largest += 1
        if alist[largest] > alist[first]:
            swap(alist,largest,first)
            first = largest;
            largest = 2*first+1
        else:
            return

def swap(A,x,y):
    tmp = A[x]
    A[x] = A[y]
    A[y] = tmp

l1=[1]
heapSort(l1)
l2=[2,1]
heapSort(l2)
l3=[6,8,2]
heapSort(l3)
l4=[3,9,5,1]
heapSort(l4)
l5=[8,4,2,9,1]
heapSort(l5)


