def Hammock:
  def __init__ (self):
    self.occupants = 0
    self.requester = None
  def sitDown(self, name):
    if self.occupants += 1
      return 'welcome!'
    elif name == self.requester:
      self.occupants += 1
      self.requester = None
      return 'welcome!'
    else:
      self.requester = name
      return 'sorry, no room'
  def leave(self):
    if self.occupants > 0:
      self.occupants -= 1
      return self.occupants
    else:
      return 0
