class Animal:
  isDog = False
  def __init__(self, name, age):
    self.name = name
    self.age = age
  def birthday(self):
    self.age = self.age + 1
  def inDogYears(self):
    if not self.isDog:
      return self.age * 7
    else:
      return self.age

print 'Animal'
print Animal

print 'snuffy = Animal(\'Aloysius Snuffleupagus\', 25)'
snuffy = Animal('Aloysius Snuffleupagus', 25)

print 'garfield = Animal(\'Garfield\', 32)'
garfield = Animal('Garfield', 32)

print 'george = Animal(\'Curious George\', 70)'
george = Animal('Curious George', 70)

print 'snoopy = Animal(\'Snoopy\', 60)'
snoopy = Animal('Snoopy', 60)

print 'print garfield.birthday'
print garfield.birthday

print 'print snoopy.inDogYears()'
print snoopy.inDogYears()

#print 'print snoopy.inDogYears()'
#print snoopy.inDogYears()

print 'print snoopy.birthday()'
print snoopy.birthday()

print 'print snoopy.inDogYears()'
print snoopy.inDogYears()

print 'snoopy.isDog = True'
snoopy.isDog = True

print 'print snoopy.inDogYears()'
print snoopy.inDogYears()

print 'snuffy.isDog = False'
snuffy.isDog = False

print 'Animal.isDog = True'
Animal.isDog = True

print 'print snuffy.inDogYears()'
print snuffy.inDogYears()

print 'print george.inDogYears()'
print george.inDogYears()
