# We have learned that search an ordered list can be done is lg(n) time using binary search, now, we are going to learn a new data structure that can be searched in O(1) time, and this is called hashing.

# Hash table: a collection of items which are stored in such a way as to make it easy to find them later. Each position of the hash table, called a slot, can hold an item and is named by an integer value starting at 0.

# e.g. slot 1, slot 2, slot 3, initially the hash table contains no items, so every slot is empty. We can implement a hash table by using a list with each element initialized to the special Python value None.
# hash function: mapping between the slot and hash table, taking any item in the collection returning an integer in the range of slot names, between 0 and m-1

def hash(a_string, table_size):
  sum = 0
  for pos in range(len(a_string)):
    sum = sum + ord(a_string[pos])

  return sum % table_size
