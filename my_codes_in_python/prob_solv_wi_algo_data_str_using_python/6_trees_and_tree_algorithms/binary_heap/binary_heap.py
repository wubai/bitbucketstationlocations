# BinaryHeap() creates a new, empty binary heap
# insert(k) adds a new item to the heap
# find_min() returns the item with the minimum key value, leaving item in the heap
# del_min() returns the item with the minimum key value, removing the item from the heap
# is_empty() returns true if the heap is empty, false otherwise
# size() returns the number of items in the heap
# build_heap(list) builds a new binary heap from a list of keys

import BinHeap
bh=BinHeap()
bh.insert(5)
bh.insert(7)
bh.insert(3)
bh.insert(11)
print(bh.del_min())
print(bh.del_min())
print(bh.del_min())
print(bh.del_min())

