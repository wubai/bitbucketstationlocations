import time

def sumOfN2(n):
  start = time.time()

  sum = 0
  for i in range(1,n+1):
    sum = sum + i

  end = time.time()

  return sum, end-start

n = 1
sumOfN2(n)
n = 100
sumOfN2(n)

def sumOfN(n):
  sum = 0
  for i in range(1,n+1):
    sum = sum + i
  return sum

print(sumOfN(10))

for i in range(5):
  print("Sum is %d required %10.7f seconds"%sumOfN(10000))
