def rev(list):
  ans = []
  if len(list) == 1:
    return list[:]
  else:
    return [list[-1]]+rev(list[0:-1])

l1 = [0,1,2,3,4,5]
a = rev(l1)
print a

l2 = [1,3,5,7,9,11,13,15]
print rev(l2)
