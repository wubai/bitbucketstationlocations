def fact(n):
  if n == 1 or n == 0:
    return 1
  else:
    return n * fact(n-1)

print fact(0)
print fact(1)
print fact(2)
print fact(3)
print fact(4)
print fact(5)

for i in range(25):
 print(fact(i))
