import math
anumber = int(raw_input("Enter a number +/-"))

if anumber < 0:
  raise RuntimeError("You can't use a negative number")
else:
  print(math.sqrt(anumber))
