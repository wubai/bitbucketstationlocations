#Question 1: Construct a class hierarchy for ppl on college: faculty, staff and students. Specify their common and differences.

class People:

  def __init__(self,name,age):
    self.name=name
    self.age=age

  # Getters
  def get_age(self):
    return self.age
  def get_name(self):
    return self.name

  # Setters
  def set_age(self,age):
    self.age = age
  def set_name(name):
    self.name = name
  
  def __str__(self):
    return "Animal: "+self.name+str(' ')+str(self.age)

class Faculty(People):
  def __init__(self,name,age):
    People.__init__(self,name,age)#: n need for the following: otherwise, error
#    self.age = age
#    self.name = name
H = Faculty('Hsin',50)
H.__str__()
print("print H using print")
print(H)

class Student(People):
  def __init__(self,name,age):
    People.__init__(self,name,age)
  
