def gcd(m,n):
 while m%n != 0:
  oldm = m
  oldn = n

  m = oldn
  n=oldm%oldn
 return n

class Fraction:
  def __init__(self,top,bottom):
#    self.num=top
#    self.den=bottom

    self.num = top//gcd(top,bottom)
    self.den = bottom//gcd(top,bottom)
    
  def show(self):
    print(self.num, '/', self.den)
  def __str__(self):
    return str(self.num) + "/" + str(self.den)
  def __add__(self,otherfraction):
    newnum = self.num * otherfraction.den + self.den * otherfraction.num
    newden = self.den *otherfraction.den
    common = gcd(newnum,newden)

    return Fraction(newnum//common,newden//common)

  def get_num(self):
    return self.num

  def get_den(self):
    return self.den

  def __ne__(self,other):
    return self.num!=other.num and self.den!=other.den
  
myfraction = Fraction(1,1)
print(myfraction.show())
print myfraction.__str__()


f1=Fraction(1,2)
print(f1.show())
print 'num'
print(f1.get_num())

print 'den'
print(f1.get_den())

f2=Fraction(3,4)
f3=f1+f2
print('f3 is')
print(f3)

f4=Fraction(2,4)
print(f4)
f5=Fraction(4,8)
print(f4.__ne__(f5))
print(f4.__ne__(f3))
