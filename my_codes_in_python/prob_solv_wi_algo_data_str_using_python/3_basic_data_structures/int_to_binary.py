def to_str(n,base):
  convert_string = "01"
  if n < base:
    return convert_string[n]
  else:
    return to_str(n / base, base) + convert_string[n%base]

for i in range(20):
  print(to_str(i,2))
