#code from MIT OCW 6.0001 Fall 2016 Lec5 Slides

def get_data(aTuple):
    nums = ()
    words = ()
    for t in aTuple:
        nums = nums + (t[0],)
        if t[1] not in words:
            words = words + (t[1],)
    min_n = min(nums)
    max_n = max(nums)
    unique_words = len(words)
    return (min_n, max_n, unique_words)

a=(‘aa’,‘cc’,‘bb’)
(x,y,z)=get_data(a)
print x
print y
print z
