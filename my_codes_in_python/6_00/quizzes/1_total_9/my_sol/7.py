# Does function meet its specification? If not, change so it's consistent with specs.

def f(L):
  """Returns a copy of the list L without modifying L."""
  result = []
  for e in L: result.append(e)
  return result

L=[1,2,3,4,5]
a=f(L)
print a

print 'yes, it meets specs'
