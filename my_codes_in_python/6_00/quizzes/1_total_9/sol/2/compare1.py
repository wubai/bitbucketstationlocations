def compare1(a,b):
    if a < 0:
      a = -a
    if b < 0:
      b = -b
    res = (a == b)
    if res:
      print a, 'and', b, 'have the same absolute value.'
    else:
      print a, 'and', b, 'have different absolute values.'
    return res

c=compare1(1,2)
print c
d=compare1(3,3)
print d
e=compare1(-5,-5)
print e
 
