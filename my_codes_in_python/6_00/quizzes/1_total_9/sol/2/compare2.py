def absolute_value(n):
    if n < 0:
      n = -n
    return n

def compare2(a,b):
    res = absolute_value(a) == absolute_value(b)
    if res:
      print a, 'and', b, 'have the same abs value.'
    else:
      print a, 'and', b, 'have different abs values.'
    return res

c = compare2(1,2)
print c
d = compare2(2,2)
print d
d = compare2(-5,-5)
print d
