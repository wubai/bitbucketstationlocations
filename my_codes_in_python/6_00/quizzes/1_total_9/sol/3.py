def f(x):
  """Given an int/a str rep. of an int, f returns the sum of its digits"""

  xs = str(x)
  if len(xs) == 1:
    return int(xs)
  n = int(xs[0]) + int(xs[1])
  if len(xs) == 2:
    return n
  else:
    return n + f(xs[2:])

a=f(2112)
print a
b=f(123456789)
print b
c=f('2468')
print c
