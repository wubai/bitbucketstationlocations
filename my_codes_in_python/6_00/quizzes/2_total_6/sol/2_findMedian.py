def findMedian(L):
  if len(L) == 0: raise ValueError("Empty list")
  copy = L[:]
  copy.sort()
  if len(copy) % 2 == 1:
    return copy[len(copy)/2]
  else:
    return(copy[len(copy)/2]+copy[len(copy)/2-1]) / 2

L=[1,2,3,4,5,6]
a=findMedian(L)
print a

#L2=[]
#a2=findMedian(L2)
#print a2

L3=[11,22,33,44,55]
b=findMedian(L3)
print b

#st=[1,a,b,c]
#dd=findMedian(st)
#print dd

