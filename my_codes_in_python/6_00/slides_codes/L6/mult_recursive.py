def mult_recursive(a,b):
    if b == 1:
        return a
    else:
        return a+mult_recursive(a,b-1)

a=1
b=2
c=mult_recursive(a,b)
print c

a=2
b=2
c=mult_recursive(a,b)
print c

a=2
b=3
c=mult_recursive(a,b)
print c
    
