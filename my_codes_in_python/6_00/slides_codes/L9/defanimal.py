class Animal(object):
    def __init__(self,age):
        self.age = age
        self.name = None
    def get_age(self):
        return self.age
    def set_age(self, newage):
        self.age = newage
    def set_name(self, newname = ""):
        self.name = newname
    def __str__(self):
        return "animal:"+str(self.name)+":"+str(self.age)



a = Animal(3)
a.get_age()
print(a.get_age())

# Python is not great at data hiding:

print(a.age)## NOT a good style
a.age= 100 ## NOT a good style
print(a.get_age())
a.extra_attribute='nothing'## NOT a good style
print(a.extra_attribute)
