tu1=(1,2,3,4,5)
tu2=("IT","math","math2","math3")
tu3="a","b","c","d"
tu4=()
tu5=(1,)

tu6=(12,34.56)
tu7=('abc','xyz')

tu8=tu6+tu7
print tu8
del tu8

tu9=tu3*3
print tu9

# Default to tuples, for any set of multiple objects, comma-separated, written without identifying symbols, i.e., brackets for lists, parentheses for tuples, etc.

print 'abc', -4.24e93, 18+6.6j,'xyz'
x,y=1,2
print "value of x , y : ", x,y

# Built-in tuple functions

print 'cmp'
print tu1
print tu2
a=cmp(tu1,tu2)
print a

print 'lengthof'
print tu3
b=len(tu3)
print b

print 'max'
print tu6
c=max(tu6)
print c

print 'min'
print tu6
d=min(tu6)
print d

print 'tuple'
print [1,2,3,4,5]
e=tuple([1,2,3,4,5])
print e
