#copied from interactive python

def newtonSqrt(n):
  global numImplem
  numImplem = 1
  approx = 0.5 * n
  better = 0.5 * (approx + n/approx)
  while better != approx:
    numImplem += 1
    approx = better
    print '{approx = '
    print approx
    better = 0.5 * (approx + n/approx)
    print 'better = }'
    print better
  print '{---'
  print 
  print 'numImplem is: '
  print numImplem
  print 
  print '---}'
  
  return approx

for i in range(1,100):
  print(newtonSqrt(i))


