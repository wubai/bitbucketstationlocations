def KelvinToFa(Temp):
  assert(Temp >= 0), "Colder than abs zero"
  return ((Temp - 273)*1.8)+32

print KelvinToFa(273)
print int(KelvinToFa(505.78))
print KelvinToFa(-5)
