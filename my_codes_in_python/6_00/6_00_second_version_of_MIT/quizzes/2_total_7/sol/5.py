def runSim(numTrials, numThrows):
  numSuccess = 0.0
  for t in range(numTrials):
    if simThrows(numThrows):
      numSuccess += 1.0
  return numSuccess/numTrials

def sim():
  return runSim(100000,4)

#simThrows returns TRUE if 4 consecutive heads happen out of 10 throws, FALSE otherwise
