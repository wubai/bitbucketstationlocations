def addVectors(v1,v2):
  """assumes v1 and v2 are lists of ints.
     returns a list containing the pointwise sum of the elements in v1 and v2. e.g., addVectors([4,5],[1,2,3]) returns [5,7,3],and addVectors([],[]) returns []. Doesn't modify inputs."""
  v1=v1[:]
  v2=v2[:]

  if len(v1)>len(v2):
    result = v1
    other = v2
  else:
    result = v2
    other = v1
  for i in range(len(other)):
    result[i] += other [i]
  return result

b=[4,5]
c=[1,2,3]
a=addVectors(b,c)
print a
