def findAll(wordList,lStr):
  """assumes: wordList is a list of words in lowercase; lStr is a str of lowercase letters; no letter occurs in lStr more than once.
  returns: a list of all the words in wordList that contain each of the letters in lStr exactly once and no letters not in lStr."""
  result=[]
  letters = sorted(lStr)
  for w in wordList:
    w = sorted(w)
    if w == letters:
      result.append(w)
  return result


wordList = ['small','cat','fox','xof']
lStr='fox'
result = findAll(wordList,lStr)
print result

