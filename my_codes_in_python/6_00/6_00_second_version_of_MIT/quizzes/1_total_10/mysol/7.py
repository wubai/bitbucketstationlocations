def logBase2(n):
  """n positive int, returns approx. log base 2 of n"""
  import math
  return math.log(n,2)

def f(n):
  """n int"""
  if n < 1:
    return
  curDigit = int(logBase2(n))
  ans = 'n = '
  while curDigit >=0:
    if n%(2**curDigit) < n:
      ans = ans + '1'
      n = n - 2**curDigit
    else:
      ans = ans + '0'
    curDigit -=1
  return ans

for i in range(2):
  print f(i)
print '-----------------'
for i in range(100):
   print int(logBase2(i+1))
