def fastFib(n,memo):
    global numCalls
    numCalls+=1
    if not n in memo:
        memo[n]=fastFib(n-1,memo)+fastFib(n-2,memo)
    return memo[n]

def fib1(n):
    memo={0:0,1:1}
    return fastFib(n,memo)


numCalls =0
n = 2
res=fib1(n)
print 'fib of', n,res,'numCalls =',numCalls

numCalls =0
n = 3
res=fib1(n)
print 'fib of', n,res,'numCalls =',numCalls

numCalls =0
n = 4
res=fib1(n)
print 'fib of', n,res,'numCalls =',numCalls

numCalls =0
n = 5
res=fib1(n)
print 'fib of', n,res,'numCalls =',numCalls


numCalls =0
n = 6
res=fib1(n)
print 'fib of', n,res,'numCalls =',numCalls


numCalls =0
n = 7
res=fib1(n)
print 'fib of', n,res,'numCalls =',numCalls
