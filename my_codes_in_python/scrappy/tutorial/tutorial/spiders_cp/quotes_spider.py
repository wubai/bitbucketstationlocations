import scrapy


class QuotesSpider(scrapy.Spider):
    name = "quotes_cp"

    def start_requests(self):
        urls = [
            'http://tieba.baidu.com/f?kw=%CF%C3%C3%C5%B4%F3%D1%A7&fr=ala0&tpl=5',
            'http://quotes.toscrape.com/page/2/',
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        page = response.url.split("/")[-2]
        filename = 'quotes-%s.html' % page
        with open(filename, 'wb') as f:
            f.write(response.body)
        self.log('Saved file %s' % filename)
