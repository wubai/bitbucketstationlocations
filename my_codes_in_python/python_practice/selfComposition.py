def square(x):
	return x*x

#a=square(4)
#print a

def selfComposition(someFunction):
	def returnFunction(args):
		return someFunction(someFunction(args))

	return returnFunction


f = selfComposition(square)
c=f(2)
print 'f=selfComposition(square)'

print c

selfComposition(lambda x: x*x)

