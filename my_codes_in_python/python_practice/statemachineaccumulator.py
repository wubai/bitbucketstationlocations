class SM:
	def start(self):
		self.state = self.startState
	def step(self, inp):
		(s,o) = self.getNextValues(self.state, inp)
		self.state = s
		return o
	def transduce(self, inputs):
		self.start()
		return [self.step(inp) for inp in inputs]

class Accumulator (SM):
	startState = 0 
	def getNextValues(self, state, inp):
		return (state + inp, state + inp)

a = Accumulator()
a.start()
a.step(7)
b = Accumulator()
b.start()
b.step(10)
a.step(-2)
print a.state, a.getNextValues(8,13), b.getNextValues(8,13)

a = Accumulator()
print 'a = Accumulator()'
b = Accumulator()
print 'b = Accumulator()'
c = Cascade(a,b)
print 'c = Cascade(a,b)'
print c.transduce([7,3,4])
print 'c.transduce([7,3,4])'


