global u1
u1 = 28
m = 0.25
b = 6

def u(n):
  if n == 1:
    return u1
  else:
    return m * u(n-1) + 6

for i in range(1,100):
  print (u(i))
