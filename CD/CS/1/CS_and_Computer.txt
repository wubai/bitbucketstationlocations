CS 

CS is the study of the theory, experimentation, and engineering that form the basis for the design and use of computers. Glossary of computer science available. Three paradigms: the first one is: science, technology, and mathematics; the second one is theory, abstraction and design. The third considers them as technocratic and scientific paradigm.

Computer

Computer, the word first used in 1613, is a device that can be instructed to carry out sequences of arithmetic or logical operations automatically. Modern computers have the ability to follow generalized sets of operations, called programs. Computers are used as control systems widely, including microwave ovens, remote controls, industrial robots, etc.

History of Computer Hardware

History of Computer Hardware: from first generation: electrical and electromechanical calculators and programmable devices to second generation: vaccum tubes, to third generation: discrete transistors and integrated circuits to fourth generation: very large scale integrated circuits. Now, theoretical and experimental sides are quantum computer, chemical computer, DNA computing, optical computer, spintronics-based computer.

Peripheral devices include input: microphone, image scanner, graphics tablet, video camera, and, of course, keyboard and mouse, etc; output: projector, printer, loudspeaker, tablet, and, of course, monitor; both input and output include: disk drive, teleprinter, etc. Computer buses include both short range and long range, long range are used in computer networking.

Other topics in computer wiki page include list of softwares, list of OS, list of programming language, professions and organisations, etc.

CS wiki
Areas of CS

Associations for Computing Machinary, abbrev as ACM, and IEEE CS identifies four areas that it consideres crucial to CS: theory of computation, algorithms and data structures, programming methodology and languages, and computer elements and architecture. In addition to these four areas, important areas are software engineering, artificial intelligence, computer networking and communication, database systems, parallel computation, distributed computation, human computer interaction, computer graphics, operating systems, and numerical and symbolic computation.


Theory of Computation

Theory of Computation: According to Peter, the fundamental question underlying CS is what can be efficiently done automated. Theory of Computation is focused on answering questions about what can be computed and what amount of resources are required to perform those computations. Automata theory is one of its branch, as well as N=NP computation complexity theory.

Summary of short overview of four areas, theoretical CS includes data structure and algorithms, theory of computation, information and coding theory, programming language theory, formal methods; Computer Systems include computer architecture and computer engineering, computer performance analysis, concurrent, parallel and distributed systems, computer networks, computer security and cryptography, and databases; computer application include computer graphics and visualization, human-computer interaction, scientific computing and AI; the last one is software engineering. It is worth to poing out that both computer applications software engineers and computer systems software engineers are among the fasted growing occupations from 2008 tp 2018. 

That is, computer applications software engineers and computer systems software engineers.

The three great insights of CS.

The first insight is about zero and one, high and low, on and off, magnetized and demagnetized. Put in words, there are only two objects that a computer has to deal with in order to represent anything.

The second insight is about move left, move right, read symbol, print 0 and print 1. Put it in words, there are only five actions that a computer has to perform in order to do anything.

The third insight is about sequence, selection and repetition. Put it in words, there are only three ways of combining these actions (into more complex ones) that are needed in order for a computer to do anything. Sequence: first do this, then do that. Selection: if, such-and-such, is the case, then do this, else, do that. Repetition: while such-and-such is the case, do this. Notice that this third insight can be further simplified with the use of goto.


